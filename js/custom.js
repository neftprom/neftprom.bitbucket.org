;($ => {
	'use strict';
	var mainApp = {
		main: () => {
			var tabs = ['#products', '#about', '#contacts'];
			var selectedTabIndex;

			$('a[href*=#]').click(function() {
				var target = this.hash;
				selectedTabIndex = tabs.indexOf(target);

				$(tabs.join(', ')).fadeOut();
				$(target).fadeIn();

				var left = $(this).position().left +
					($(this).width() / 2) -
					($('#selected').width() / 2);

				$('#selected').animate({left: left});
			});

			$(window).resize($.debounce(300, () => {
				$('a[href*=' + window.location.hash + ']').click();
			}));

			$(document).keydown($.debounce(300, function(e) {
				console.log(e);
				var sign = e.which === 39 ? +1 : e.which === 37 ? -1 : 0;
				if (!sign) return;
				selectedTabIndex = (selectedTabIndex + sign + tabs.length) % tabs.length;
				$('a[href*=' +  tabs[selectedTabIndex] + ']').click();
			}));
		},
		changeBackground: (() => {
			var backgrounds = [
				'background1.jpg',
				'background2.jpg',
				'background3.jpg'
			];
			var i = 0;

			return () => {
				var $tile = $('.right');
				$tile.fadeOut({
					complete: () => {
						$tile.css({
							background: 'linear-gradient(to right, #000, rgba(0, 0, 0, 0)),' +
								'url(/img/' + backgrounds[i] + ') no-repeat 100% 100%',
								'background-size': 'cover'
						});
						$tile.fadeIn();
					}
				});

				i = ++i % 3;
			};
		})(),
		initialization: () => {
			mainApp.main();

			$('#selected').fadeIn();

			if (window.location.hash) {
				$('a[href*=' + window.location.hash + ']').click();
			} else {
				$('a[href*=#products]').click();
			}
		}
	};
	$(document).ready(() => {
		mainApp.initialization();
	});
	$(window).load(() => {
		mainApp.changeBackground();

		setInterval(() => {
			mainApp.changeBackground();
		}, 5000);
	});
})(jQuery);
